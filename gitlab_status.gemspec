Gem::Specification.new do |s|
  s.name        = 'gitlab_status'
  s.version     = '0.0.1'
  s.date        = '2017-11-04'
  s.summary     = "GitLab status"
  s.description = "A simple GitLab response gem"
  s.authors     = ["Serafeim Tigkiridis"]
  s.email       = 'serafeimgr@gmail.com'
  s.files       = ["lib/gitlab_status.rb"]
  s.license       = 'MIT'
end
