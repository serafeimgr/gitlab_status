require 'optparse'
require 'nokogiri'
require 'httparty'

GITLAB_STATUS = "https://status.gitlab.com/"


Options = Struct.new(:start)

class Parser
  def self.parse()
    args = Options.new()

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: gitlab_status.rb [options]"

      opts.on("-s", "--start", "Starts the GitLab status report.") do |s|
        args.start = s
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end

    opt_parser.parse!()
    return args
  end
end

class GitLab
  def self.get_gitlab_response(mock=nil, response=nil)
    unless mock
      response = HTTParty.get(GITLAB_STATUS)
    end
    doc = Nokogiri::HTML(response)
    response_times = doc.css('div div.col', 'h1')
    labels = doc.css('div div.col', 'h5')
    # puts "GitLab Status"
    return Hash[labels.zip(response_times)]
    # Hash[labels.zip(response_times)].each do |key, value|
    #   puts "#{key.content} | #{value.content}"
  end

  def self.print_response(response_hash)
    stmt = "GitLab Status\n"
    response_hash.each do |key, value|
      stmt += "#{key.content} | #{value.content}\n"
    end
    puts stmt
    return stmt
  end
end

args = Parser.parse()
if args.start
  GitLab.print_response(GitLab.get_gitlab_response())
end
