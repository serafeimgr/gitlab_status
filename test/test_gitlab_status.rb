require 'test/unit'
require 'gitlab_status'

class GitLabStatusTest < Test::Unit::TestCase
  def test_get_gitlab_response
    file = File.open('test/status.gitlab.com.html')
    data = file.read
    file.close
    expected_stmt = 'GitLab Status'+ "\n"
    expected_stmt += 'OK latency | 0.093436s'+ "\n"
    expected_stmt += 'OK ssh response time | 0.192282s'+ "\n"
    expected_stmt += 'OK http project response time | 1.092303s' + "\n"
    expected_stmt += 'OK http response time | 0.402189s' + "\n"
    assert_equal  expected_stmt, GitLab.print_response(GitLab.get_gitlab_response(true, data))
  end
end
